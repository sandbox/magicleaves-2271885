CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
MagicLeaves - Commerce EST Direct Payment Module
 
This module is an extension of drupal commerce to provide a Credit Card 
payment method of EST Banks in Turkey. Currently this module supports one
vpos at a time. 
Supported banks are 
Akbank, Halkbank, İş Bank, Finansbank, Kuveyttürk, Anadolubank, Denizbank,
Citibank, TEB, Cardplus, IngBank. 

Before you start note that this module does not support 3D Secure model. 
For 3D Support please visit http://magicleaves.com and just submit a message. 

The transactions made by this module are logged to the database. 
If core "dblog" module enabled you can check the logs from 
"Recent log messages" under
"Site Reports" (http://www.yoursite.com/?q=admin/reports/dblog).

REQUIREMENTS 
------------ 
 * Drupal module: Commerce. 
 * Drupal module: Libraries. 
 * One VirtualPOS aggrement and the account details that is taken from your
   VPOS supplier bank. 
   Account details are: 
    - ClientID 
    - API username 
    - API password 
    - API Service URL 

INSTALLATION 
------------ 
 * Extract the module to your modules folder as usual. 
 * Download the required library package from 
magicleaves.com/sites/default/files/community/magicleaves_vpos_lib_d7_v0.1.zip
 * Extract the library package into your libraries folder. 
   (sites/all/libraries/magicleaves_vpos_lib/[3-files-here]).
 * Enable module as usual from your modules administration page. 

CONFIGURATION
-------------
 * From menu go to Store settings->Advanced store settings->Payment methods and 
   edit "MagicLeaves EST Direct Payment" rule. In here edit the 
   "Enable payment method: MagicLeaves EST Direct Payment" action with the 
   information provided from your virtual pos supplier. Click Save. 
 * Go to your Payment Methods administration page and enable the 
   "MagicLeaves EST Direct Payment" method. 
 * The payment option will now show on the chackout page. 

MAINTAINERS
-----------
Current maintainers:
 * magicleaves - https://www.drupal.org/user/2900433
